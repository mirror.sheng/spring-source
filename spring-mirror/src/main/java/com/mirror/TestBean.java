package com.mirror;

import com.mirror.config.MirrorConfig;
import com.mirror.service.HelloService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author shenggangfeng
 * @date 2021-05-18 23:46
 */
public class TestBean {

	public static void main(String[] args) {
		AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext(MirrorConfig.class);
		HelloService helloService = annotationConfigApplicationContext.getBean(HelloService.class);
		helloService.sayHello();
	}
}
