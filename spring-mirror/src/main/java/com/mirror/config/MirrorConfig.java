package com.mirror.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author shenggangfeng
 * @description
 * @date 2021-05-18 23:44
 */
@Configuration
@ComponentScan("com.mirror")
public class MirrorConfig {
}
