package com.mirror.service;

/**
 * @author shenggangfeng
 * @description
 * @date 2021-05-18 22:49
 */
public interface HelloService {

	void sayHello();
}
