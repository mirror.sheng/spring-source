package com.mirror.service.impl;

import com.mirror.service.HelloService;
import com.mirror.service.HelloWordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * @author shenggangfeng
 * @date 2021-05-18 22:05
 */
@Service
public class HelloServiceImpl implements HelloService {

	@Autowired
	private HelloWordService helloWordService;

	public HelloServiceImpl() {
		System.out.println("HelloServiceImpl......");
	}

	@PostConstruct
	public void initMethod() {
		System.out.println("initMethod......");
	}

	@Override
	public void sayHello() {
		System.out.println("hello spring!");
	}
}
