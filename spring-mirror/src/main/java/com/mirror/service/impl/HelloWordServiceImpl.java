package com.mirror.service.impl;

import com.mirror.service.HelloService;
import com.mirror.service.HelloWordService;
import org.springframework.stereotype.Service;

/**
 * @author shenggangfeng
 * @description
 * @date 2021-05-18 22:42
 */
@Service
public class HelloWordServiceImpl implements HelloWordService {

	private HelloService helloService;

	public HelloWordServiceImpl(HelloService helloService) {
		this.helloService = helloService;
		System.out.println("HelloWordServiceImpl......");
	}


}
